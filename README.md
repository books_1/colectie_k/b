# B

## Content

```
./B. A. Paris:
B. A. Paris - In spatele usilor inchise 1.0 '{Politista}.docx

./B. Blanchard:
B. Blanchard - Arta tacerii 0.99 '{DezvoltarePersonala}.docx

./B. G. Kuznetov:
B. G. Kuznetov - Ratiune si fiintare 0.8 '{Filozofie}.docx

./B. Michael Radburn:
B. Michael Radburn - Cargo 1.0 '{SF}.docx

./B. T. Schmidt & H. Roberds:
B. T. Schmidt & H. Roberds - Predator - Moarte la Rio 1.0 '{SF}.docx

./B. Traven:
B. Traven - Comoara din Sierra Madre 1.0 '{Western}.docx
B. Traven - Vasul mortii 0.8 '{Literatura}.docx

./Baird T. Spalding:
Baird T. Spalding - Ultimele cuvinte 0.9 '{Spiritualitate}.docx
Baird T. Spalding - Viata si invataturile maestrilor din extremul orient 1.0 '{Spiritualitate}.docx

./Bajgent L. Lincoln:
Bajgent L. Lincoln - Sangele Sfant si Sfantul Graal 0.7 '{MistersiStiinta}.docx

./Baldwin Wolf:
Baldwin Wolf - Furia supravietuirii 1.0 '{ActiuneRazboi}.docx

./Balin Feri:
Balin Feri - A sasea porunca 0.99 '{SF}.docx
Balin Feri - Casa de vacanta 0.99 '{SF}.docx
Balin Feri - Consolarea 0.99 '{SF}.docx
Balin Feri - Creator 0.99 '{SF}.docx
Balin Feri - Damnatul 0.9 '{SF}.docx
Balin Feri - Descoperirea secolului 0.6 '{SF}.docx
Balin Feri - Divina tragedie 0.9 '{SF}.docx
Balin Feri - Eternele roluri 0.99 '{SF}.docx
Balin Feri - Extrem 0.99 '{SF}.docx
Balin Feri - Omenirea, nu prea bine 0.99 '{SF}.docx
Balin Feri - Overwrite 0.99 '{SF}.docx
Balin Feri - Pietre pretioase 0.99 '{SF}.docx
Balin Feri - Planeta zeilor 0.99 '{SF}.docx
Balin Feri - Restrictia 0.99 '{SF}.docx
Balin Feri - Sa nu te temi 0.8 '{SF}.docx

./Banana Yoshimoto:
Banana Yoshimoto - Kitchen 1.0 '{Literatura}.docx
Banana Yoshimoto - N. P. 1.0 '{Literatura}.docx

./Barbara Andrews:
Barbara Andrews - Promisiuni furate 1.0 '{Romance}.docx

./Barbara Boswell:
Barbara Boswell - Drumuri impletite 0.6 '{Romance}.docx
Barbara Boswell - Lectii particulare 0.9 '{Romance}.docx
Barbara Boswell - Micile urmari 1.0 '{Romance}.docx
Barbara Boswell - Obstacole dragi 0.99 '{Romance}.docx
Barbara Boswell - Tentatie arzatoare 1.0 '{Romance}.docx

./Barbara Cartland:
Barbara Cartland - Casa fericirii 0.99 '{Romance}.docx
Barbara Cartland - Doar dragoste 0.9 '{Romance}.docx
Barbara Cartland - Iubire, minciuni, casatorie 0.99 '{Romance}.docx
Barbara Cartland - Luminile dragostei 0.99 '{Romance}.docx
Barbara Cartland - Nisipul fierbinte din Hawaii 0.99 '{Romance}.docx
Barbara Cartland - Patima 0.7 '{Romance}.docx
Barbara Cartland - Povara secretului 1.0 '{Romance}.docx
Barbara Cartland - Sarutul diavolului 1.0 '{Romance}.docx
Barbara Cartland - Seducatoarea 0.7 '{Romance}.docx
Barbara Cartland - Stapanii Coastei 1.0 '{Romance}.docx
Barbara Cartland - Vraja Parisului 0.7 '{Romance}.docx

./Barbara Dooley:
Barbara Dooley - Prejudecati 0.99 '{Dragoste}.docx
Barbara Dooley - Secretul lui Megan 0.99 '{Dragoste}.docx

./Barbara Erskine:
Barbara Erskine - Lady Hay 1.0 '{Aventura}.docx

./Barbara Ewing:
Barbara Ewing - Rosetta 1.0 '{Dragoste}.docx

./Barbara Marciniak:
Barbara Marciniak - Extras din mesajele primite din constelatia Pleiadelor 0.99 '{Spiritualitate}.docx

./Barbara Mccauley:
Barbara Mccauley - Nopti de foc 1.0 '{Romance}.docx

./Barbara Paul:
Barbara Paul - Nocturna pentru 3 pumnale 0.7 '{Politista}.docx

./Barbara Perkins:
Barbara Perkins - Dragoste si spini 0.99 '{Dragoste}.docx

./Barbara Steel:
Barbara Steel - Vrajitorul din Portland 0.99 '{Dragoste}.docx

./Barbara Taylor Bradford:
Barbara Taylor Bradford - Cheia trecutului 0.6 '{Romance}.docx
Barbara Taylor Bradford - Remember 0.8 '{Romance}.docx

./Barbara York:
Barbara York - Intalnire intamplatoare 0.99 '{Dragoste}.docx
Barbara York - Sansa dragostei 0.99 '{Dragoste}.docx

./Barbu Apelevianu:
Barbu Apelevianu - Edison 1.0 '{Biografie}.docx

./Barbu Slatineanu:
Barbu Slatineanu - Sub semnul palosului 2.0 '{IstoricaRo}.docx

./Barbu Stefanescu Delavrancea:
Barbu Stefanescu Delavrancea - Basme 0.9 '{BasmesiPovesti}.docx
Barbu Stefanescu Delavrancea - Bunica 0.9 '{Literatura}.docx
Barbu Stefanescu Delavrancea - Bunicul 0.9 '{Literatura}.docx
Barbu Stefanescu Delavrancea - Povesti 0.9 '{BasmesiPovesti}.docx
Barbu Stefanescu Delavrancea - Sultanica 1.0 '{Literatura}.docx
Barbu Stefanescu Delavrancea - Trilogia Moldovei - V1 Apus de soare 0.99 '{Literatura}.docx
Barbu Stefanescu Delavrancea - Trilogia Moldovei - V2 Luceafarul 1.0 '{Literatura}.docx
Barbu Stefanescu Delavrancea - Trilogia Moldovei - V3 Viforul 1.0 '{Literatura}.docx

./Barry B. Longyear:
Barry B. Longyear - Dusmanul 4.0 '{SF}.docx

./Barry Eisler:
Barry Eisler - Asasinul din Tokio - V1 Tintele 1.0 '{Thriller}.docx
Barry Eisler - Asasinul din Tokio - V2 Razbunarea 1.0 '{Thriller}.docx
Barry Eisler - Asasinul din Tokio - V3 Tradarea 1.0 '{Thriller}.docx
Barry Eisler - Asasinul din Tokio - V4 Jocurile caintei 1.0 '{Thriller}.docx
Barry Eisler - Asasinul din Tokio - V5 Furia samuraiului 1.0 '{Thriller}.docx
Barry Eisler - Asasinul din Tokio - V6 Complot dejucat la Rotterdam 1.0 '{Thriller}.docx
Barry Eisler - Asasinul din Tokio - V7 Conspiratie la Casa Alba 1.0 '{Thriller}.docx
Barry Eisler - Asasinul din Tokio - V8 Prima misiune in Tokio 1.0 '{Thriller}.docx
Barry Eisler - Operatiunea Ochiul lui Dumnezeu 1.0 '{Thriller}.docx

./Barry N. Malzberg:
Barry N. Malzberg - Conexiunea 0.99 '{SF}.docx
Barry N. Malzberg - Coridoare 1.0 '{SF}.docx
Barry N. Malzberg - Pentru a insemna timpul 1.0 '{SF}.docx

./Bart D. Ehrman:
Bart D. Ehrman - Adevar si fictiune in Codul lui Da Vinci 0.99 '{Spiritualitate}.docx

./Basilea Schlink:
Basilea Schlink - New Age dintr-o perspectiva biblica 0.9 '{Spiritualitate}.docx

./Basme:
Basme - Basme si povesti din China 1.0 '{BasmesiPovesti}.docx
Basme - Basme si povestiri japoneze 1.0 '{BasmesiPovesti}.docx
Basme - Fratii Liu 1.0 '{BasmesiPovesti}.docx
Basme - Povestea tunetului Rai-Taro 1.0 '{BasmesiPovesti}.docx
Basme - Zana Onda 1.0 '{BasmesiPovesti}.docx

./Bauby Jean Dominique:
Bauby Jean Dominique - Scafandrul si fluturele 0.99 '{Literatura}.docx

./Beatrice Kiseleff:
Beatrice Kiseleff - Sfidarea paranormalului 0.99 '{Spiritualitate}.docx

./Beaumarchais:
Beaumarchais - Barbierul din Sevilla 1.0 '{Teatru}.docx
Beaumarchais - Nunta lui Figaro 1.0 '{Teatru}.docx

./Bebe Anastasie Mate:
Bebe Anastasie Mate - Stapanul sau sclavul femeilor 0.7 '{Necenzurat}.docx

./Bebe Mihaescu:
Bebe Mihaescu - A face dragoste aproape perfect 1.0 '{Necenzurat}.docx

./Becca Fitzpatrick:
Becca Fitzpatrick - Ingerul Noptii - V1 Ingerul noptii 0.99 '{Vampiri}.docx
Becca Fitzpatrick - Ingerul Noptii - V2 Crescendo 1.0 '{Vampiri}.docx
Becca Fitzpatrick - Ingerul Noptii - V3 Tacere 1.0 '{Vampiri}.docx

./Becca Slipper:
Becca Slipper - Pariu pe iubire 0.99 '{Romance}.docx
Becca Slipper - Peste ani 0.9 '{Romance}.docx

./Becky Albertalli:
Becky Albertalli - Simon si planul Homo Sapiens 0.7 '{Literatura}.docx

./Becky Chambers:
Becky Chambers - Un lung drum spre o planeta mica si furioasa 1.0 '{SF}.docx

./Bedri Cetin:
Bedri Cetin - Energia universala 0.7 '{Spiritualitate}.docx

./Bejamin Iscarioteanu:
Bejamin Iscarioteanu - Evanghelia dupa Iuda 1.0 '{Spiritualitate}.docx

./Belinda Cannone:
Belinda Cannone - Triumful prostiei 0.8 '{Diverse}.docx

./Belle de Jour:
Belle de Jour - Aventurile intime ale unei prostituate de lux londoneze 1.0 '{Literatura}.docx

./Ben Ami:
Ben Ami - Anumis impotriva gandirii 0.99 '{SF}.docx
Ben Ami - D(o)ar zei! 2.0 '{SF}.docx
Ben Ami - Doua secunde si-un scop 0.99 '{SF}.docx
Ben Ami - Eternitate 2.0 '{SF}.docx
Ben Ami - Inger noroios 2.0 '{SF}.docx
Ben Ami - Rasplata 2.0 '{SF}.docx
Ben Ami - Suspendati intr-o raza de soare 1.0 '{SF}.docx
Ben Ami - Trinidad 0.9 '{SF}.docx

./Ben Bennett:
Ben Bennett - Cina in paradis 1.0 '{Romance}.docx

./Ben Bova:
Ben Bova - Powersat 3.1 '{SF}.docx

./Ben Carson:
Ben Carson - Imaginea de ansamblu 0.99 '{Diverse}.docx
Ben Carson - Maini inzestrate 1.0 '{Literatura}.docx

./Benedek Totth:
Benedek Totth - Meci nul 1.0 '{Literatura}.docx

./Benedetta Craveri:
Benedetta Craveri - Amante si regine 0.99 '{AventuraIstorica}.docx

./Ben Elton:
Ben Elton - Crima in direct 1.0 '{Thriller}.docx

./Ben Goldacre:
Ben Goldacre - Pseudostiinta 1.0 '{MistersiStiinta}.docx

./Bengt Danielsson:
Bengt Danielsson - Insulele pierdute 1.0 '{Aventura}.docx

./Benjamin Black:
Benjamin Black - Blonda cu ochi negri 1.0 '{Politista}.docx

./Benjamin Franklin:
Benjamin Franklin - Autobiografie 1.0 '{Biografie}.docx

./Benjamin Klein:
Benjamin Klein - Iti cunosti tu ingerul tau 0.9 '{Spiritualitate}.docx
Benjamin Klein - Nici vorba de moarte 0.7 '{Spiritualitate}.docx

./Benjamin Ludwig:
Benjamin Ludwig - Ginny Moon 1.0 '{Diverse}.docx

./Benjamin Mee:
Benjamin Mee - Avem un Zoo 1.0 '{Literatura}.docx

./Benjamin Read:
Benjamin Read - Lumea din miezul noptii 1.0 '{Literatura}.docx

./Benjamin Rosenbaum:
Benjamin Rosenbaum - Casa de dincolo de cerul vostru 2.0 '{Diverse}.docx
Benjamin Rosenbaum - Pornirea ceasului 0.9 '{Diverse}.docx

./Benjamin Spock:
Benjamin Spock - Esential pentru bebelusul tau 1.0 '{Sanatate}.docx
Benjamin Spock - Ingrijirea sugarului si a copilului 1.0 '{Sanatate}.docx

./Ben Jeapes:
Ben Jeapes - Ofuri de soft-uri 0.99 '{SF}.docx

./Benoit Buteurtre:
Benoit Buteurtre - Fetita si tigara 0.9 '{Literatura}.docx

./Ben Okri:
Ben Okri - Drumul infometat 0.9 '{Literatura}.docx
Ben Okri - In Arcadia 2.0 '{Literatura}.docx

./Ben Rice:
Ben Rice - Pobby si Dingan 0.99 '{Literatura}.docx

./Ben Winters:
Ben Winters - Ultimul politist 1.0 '{Politista}.docx

./Bernard Beckett:
Bernard Beckett - Geneza 1.0 '{SF}.docx

./Bernard Clavel:
Bernard Clavel - Stapanul fluviului 1.0 '{Aventura}.docx

./Bernard Cornwell:
Bernard Cornwell - Hotul de spanzuratoare 1.0 '{AventuraIstorica}.docx
Bernard Cornwell - In Cautarea Graalului - V1 Harlequin 1.0 '{AventuraIstorica}.docx
Bernard Cornwell - In Cautarea Graalului - V2 Vagabond 1.0 '{AventuraIstorica}.docx
Bernard Cornwell - In Cautarea Graalului - V3 Eretic 1.0 '{AventuraIstorica}.docx
Bernard Cornwell - Ultimul Regat - V1 Ultimul regat 2.0 '{AventuraIstorica}.docx
Bernard Cornwell - Ultimul Regat - V2 Calaretul mortii 2.0 '{AventuraIstorica}.docx
Bernard Cornwell - Ultimul Regat - V3 Stapanii nordului 2.0 '{AventuraIstorica}.docx
Bernard Cornwell - Ultimul Regat - V4 Cantecul sabiei 2.0 '{AventuraIstorica}.docx
Bernard Cornwell - Ultimul Regat - V5 Tinuturi in flacari 2.0 '{AventuraIstorica}.docx
Bernard Cornwell - Ultimul Regat - V6 Moartea regilor 2.0 '{AventuraIstorica}.docx
Bernard Cornwell - Ultimul Regat - V7 Lordul pagan 2.0 '{AventuraIstorica}.docx
Bernard Cornwell - Ultimul Regat - V8 Tronul gol 2.0 '{AventuraIstorica}.docx
Bernard Cornwell - Ultimul Regat - V9 Razboinicii furtunii 2.0 '{AventuraIstorica}.docx
Bernard Cornwell - Ultimul Regat - V10 Purtatorul tortei 2.0 '{AventuraIstorica}.docx

./Bernard Dagenais:
Bernard Dagenais - Campania de relatii publice 0.9 '{DezvoltarePersonala}.docx

./Bernard du Boucheron:
Bernard du Boucheron - Sarpele scurt 1.0 '{AventuraIstorica}.docx

./Bernard Knight:
Bernard Knight - Mistere Medievale - V1 Dincolo de liman 2.0 '{AventuraIstorica}.docx
Bernard Knight - Potirul otravit 0.99 '{Politista}.docx

./Bernard Malamud:
Bernard Malamud - Lada fermecata 0.7 '{Literatura}.docx

./Bernard Minier:
Bernard Minier - Martin Servaz - V1 Inghetat 1.0 '{Politista}.docx

./Bernardo Guimaraes:
Bernardo Guimaraes - Cautatorul de diamante 0.6 '{AventuraIstorica}.docx

./Bernard Picton:
Bernard Picton - Expertul 1.0 '{Politista}.docx
Bernard Picton - Firul evidentei 1.0 '{Politista}.docx

./Bernard Werber:
Bernard Werber - Arborele scenariilor posibile si alte povestiri 2.0 '{Aventura}.docx
Bernard Werber - Imperiul ingerilor 0.99 '{Aventura}.docx
Bernard Werber - Povestiri 0.8 '{Aventura}.docx
Bernard Werber - Thanatonautii 1.0 '{Aventura}.docx
Bernard Werber - V1 Furnicile 5.0 '{Aventura}.docx
Bernard Werber - V2 Ziua furnicilor 5.0 '{Aventura}.docx
Bernard Werber - V3 Revolutia furnicilor 5.0 '{Aventura}.docx

./Bernhard Schlink:
Bernhard Schlink - Cititorul 1.0 '{Thriller}.docx
Bernhard Schlink - Crima lui Selb 1.0 '{Thriller}.docx
Bernhard Schlink - Evadari din iubire 1.0 '{Thriller}.docx
Bernhard Schlink - Intoarcerea acasa 1.0 '{Thriller}.docx
Bernhard Schlink - Weekendul 0.99 '{Thriller}.docx

./Bert F. Island:
Bert F. Island - Jocul s-a sfarsit 1.0 '{Politista}.docx

./Bertha Dudde:
Bertha Dudde - Cine este Dumnezeu, cine este Lucifer, ce este Omul 0.9 '{Spiritualitate}.docx
Bertha Dudde - Cine este Dumnezeu 0.99 '{Spiritualitate}.docx
Bertha Dudde - Planul de mantuire al lui Dumnezeu 0.99 '{Spiritualitate}.docx

./Bertram Chandler:
Bertram Chandler - Cusca 1.0 '{SF}.docx
Bertram Chandler - Jumatate de pereche 0.99 '{SF}.docx

./Bertrand Russell:
Bertrand Russell - In cautarea fericirii 0.8 '{Filozofie}.docx
Bertrand Russell - Religie si stiinta 0.7 '{Filozofie}.docx

./Beryl Bainbridge:
Beryl Bainbridge - Urmand-o pe Hariet 0.99 '{Dragoste}.docx

./Bess Norton:
Bess Norton - Adiere de primavara 0.99 '{Romance}.docx
Bess Norton - Viata de medic 0.99 '{Romance}.docx

./Beth Brookes:
Beth Brookes - Dorinta salbatica 0.99 '{Dragoste}.docx

./Bethea Creese:
Bethea Creese - Domnita din Cipru 0.99 '{Dragoste}.docx

./Beth O'Leary:
Beth O'Leary - Dragoste in contratimp 1.0 '{Dragoste}.docx

./Beth Perkins:
Beth Perkins - Departe de tine 0.99 '{Romance}.docx

./Betta Longforth:
Betta Longforth - Dragoste tradata 0.99 '{Dragoste}.docx

./Betty Beaty:
Betty Beaty - Iubire nestinsa 0.99 '{Dragoste}.docx

./Betty Holbrook & D. W. Holbrook:
Betty Holbrook & D. W. Holbrook - Inainte de a spune 0.7 '{DezvoltarePersonala}.docx

./Betty Mahmoody:
Betty Mahmoody - Patsy Heymans - V1 Numai cu fiica mea 1.0 '{Diverse}.docx
Betty Mahmoody - Patsy Heymans - V2 din dragoste pentru un copil 1.0 '{Diverse}.docx
Betty Mahmoody - Patsy Heymans - V3 Rapiti 1.0 '{Diverse}.docx

./Betty Watson:
Betty Watson - Barbatul din salbaticie 0.99 '{Romance}.docx

./Beverly Sommers:
Beverly Sommers - O mica schimbare 0.99 '{Romance}.docx

./Bianca Mcgreen:
Bianca Mcgreen - Destine 0.99 '{Dragoste}.docx

./Bianca Mihaela Dobrescu:
Bianca Mihaela Dobrescu - Ecce homo 0.9 '{Literatura}.docx

./Biblia:
Biblia - Noua traducere romaneasca 1.0 '{Religie}.docx

./Bill Baldwin:
Bill Baldwin - Pilotul galactic 1.0 '{SF}.docx

./Bill Clinton & James Patterson:
Bill Clinton & James Patterson - In lipsa presedintelui 1.0 '{Politica}.docx

./Bill Curtis:
Bill Curtis - Mogadiscio in flacari 1.0 '{ActiuneComando}.docx

./Bill Fawcett:
Bill Fawcett - Asediul Aristei 2.0 '{SF}.docx
Bill Fawcett - Razboiul stelelor indepartate 2.0 '{SF}.docx

./Bill Mccay:
Bill Mccay - Stargate - V1 Razvratirea 1.0 '{SF}.docx

./Bill Napier:
Bill Napier - Tripticul 1.0 '{Suspans}.docx

./Bill Vaughan:
Bill Vaughan - Reteta pentru o planeta locuibila 5.0 '{SF}.docx

./Billy Graham:
Billy Graham - Lumea in flacari 0.8 '{Spiritualitate}.docx

./Bjornstjerne Bjornson:
Bjornstjerne Bjornson - Tomas Rendalen 0.6 '{Literatura}.docx

./Blaga Mihoc:
Blaga Mihoc - Peregrinul si umbra 0.7 '{Biografie}.docx

./Blaise Pascal:
Blaise Pascal - Cugetari 1.0 '{Filozofie}.docx

./Blake Crouch:
Blake Crouch - Materia intunecata 1.0 '{Thriller}.docx
Blake Crouch - Wayward Pines - V1 Orasul din munti 1.0 '{Thriller}.docx
Blake Crouch - Wayward Pines - V2 Seriful 1.0 '{Thriller}.docx
Blake Crouch - Wayward Pines - V3 Ultimul oras 1.0 '{Thriller}.docx

./Blue Balliet:
Blue Balliet - Codul Wright 1.0 '{Tineret}.docx
Blue Balliet - Pe urmele lui Vermeer 1.0 '{Tineret}.docx

./Blythe Bradley:
Blythe Bradley - Magia unui barbat 0.9 '{Dragoste}.docx
Blythe Bradley - Sa iubesti un strain 0.99 '{Romance}.docx

./Bob Mayer:
Bob Mayer - Sinbat 1.0 '{ActiuneComando}.docx

./Bob Ottum Jr.:
Bob Ottum Jr. - Zgomot pentru nimic 0.99 '{SF}.docx

./Bob Shaw:
Bob Shaw - Astronautii zdrentarosi 1.0 '{SF}.docx
Bob Shaw - Lumina unor zile de-altadata 0.99 '{SF}.docx

./Bogdan Burileanu:
Bogdan Burileanu - A treia dimensiune 0.8 '{Literatura}.docx

./Bogdan Ficeac:
Bogdan Ficeac - India, o poarta spre eternitate 0.6 '{Diverse}.docx
Bogdan Ficeac - Tehnici de manipulare 1.0 '{Psihologie}.docx

./Bogdan Gheorghiu:
Bogdan Gheorghiu - Cai incurcate 0.2 '{SF}.docx
Bogdan Gheorghiu - Cyclone 0.99 '{SF}.docx
Bogdan Gheorghiu - Hax Grid 0.6 '{SF}.docx

./Bogdan Ghibu:
Bogdan Ghibu - Miza unui joc dublu 1.0 '{Politista}.docx

./Bogdan Petriceicu Hasdeu:
Bogdan Petriceicu Hasdeu - Ioan voda cel cumplit 1.0 '{ClasicRo}.docx
Bogdan Petriceicu Hasdeu - Micuta 1.0 '{ClasicRo}.docx
Bogdan Petriceicu Hasdeu - Razvan si Vidra 1.0 '{ClasicRo}.docx
Bogdan Petriceicu Hasdeu - Ursita 1.0 '{ClasicRo}.docx

./Bogdan Stanoiu:
Bogdan Stanoiu - Spatii complementare 0.8 '{Diverse}.docx

./Bogdan Suceava:
Bogdan Suceava - Batalii si mesagii 0.99 '{Poezie}.docx
Bogdan Suceava - Cubul Rubik cu zece patrate albastre 0.99 '{ProzaScurta}.docx
Bogdan Suceava - Descompunerea patriei in particule elementare 0.99 '{ProzaScurta}.docx
Bogdan Suceava - Imperiul generalilor tarzii 0.99 '{Literatura}.docx
Bogdan Suceava - Ireparabile 0.99 '{ProzaScurta}.docx
Bogdan Suceava - Istoria de la Al Waqbah 0.99 '{ProzaScurta}.docx
Bogdan Suceava - Miraculoasa istorie a inaltarii omului in vazduh 0.99 '{ProzaScurta}.docx
Bogdan Suceava - Poveste cu un acordeon 0.99 '{ProzaScurta}.docx
Bogdan Suceava - Praf de carbune 0.99 '{ProzaScurta}.docx
Bogdan Suceava - Sa auzi forma unei tobe 0.9 '{ProzaScurta}.docx
Bogdan Suceava - Tata a vrut televizor sambata seara 0.99 '{ProzaScurta}.docx
Bogdan Suceava - Ultimul diamant al coroanei 0.9 '{ProzaScurta}.docx
Bogdan Suceava - Un obiect tensionat cu neasteptate aplicatii 0.9 '{ProzaScurta}.docx
Bogdan Suceava - Ursi experimentali 0.9 '{ProzaScurta}.docx

./Bogdan Tabarcea:
Bogdan Tabarcea - Arca 0.9 '{ProzaScurta}.docx

./Bogdan Teodorescu:
Bogdan Teodorescu - 5 Milenii de manipulare 2.0 '{Istorie}.docx
Bogdan Teodorescu - Calatorii in 24 tari 1.0 '{Calatorii}.docx
Bogdan Teodorescu - Cea mai buna dintre lumile posibile 0.9 '{Politica}.docx
Bogdan Teodorescu - Libertate 1.0 '{Literatura}.docx
Bogdan Teodorescu - Marketing Politic 0.9 '{Politica}.docx
Bogdan Teodorescu - Si vom muri cu totii in chinuri 1.0 '{Politica}.docx
Bogdan Teodorescu - Spada 2.0 '{Literatura}.docx

./Bogdan Tudor Bucheru:
Bogdan Tudor Bucheru - Comerciantii 0.99 '{SF}.docx
Bogdan Tudor Bucheru - Intre lacuri 0.99 '{SF}.docx

./Bogomil Rainov:
Bogomil Rainov - Frumos e timpul mohorat 0.8 '{Politista}.docx
Bogomil Rainov - Inspectorul si noaptea 1.0 '{Politista}.docx
Bogomil Rainov - Marea plictiseala 1.0 '{Politista}.docx

./Bohumil Hrabal:
Bohumil Hrabal - L-am servit pe regele Angliei 0.7 '{Literatura}.docx
Bohumil Hrabal - Trenuri cu prioritate 2.0 '{Literatura}.docx

./Boileau Narcejac:
Boileau Narcejac - Complot 1.0 '{Politista}.docx

./Bojin Pavlovski:
Bojin Pavlovski - Duva 1.0 '{Literatura}.docx
Bojin Pavlovski - Ipocritul 1.0 '{Literatura}.docx
Bojin Pavlovski - Western Australia 1.0 '{Literatura}.docx

./Boleslaw Prus:
Boleslaw Prus - Faraonul 1.0 '{AventuraIstorica}.docx
Boleslaw Prus - Papusa V1 1.0 '{Literatura}.docx
Boleslaw Prus - Papusa V2 1.0 '{Literatura}.docx

./Bora Cosic:
Bora Cosic - Rolul familiei mele in revolutia mondiala 1.0 '{Diverse}.docx

./Boris Akunin:
Boris Akunin - Azazel 1.0 '{Politista}.docx
Boris Akunin - Gambit turcesc 0.99 '{Politista}.docx
Boris Akunin - Leviatan 0.99 '{Politista}.docx
Boris Akunin - Moartea lui Ahile 1.0 '{Politista}.docx

./Boris Beldnai:
Boris Beldnai - Fetele 0.8 '{Literatura}.docx

./Boris Craciun:
Boris Craciun - Regii si reginele Romaniei 1.0 '{Istorie}.docx

./Boris Cyrulnik:
Boris Cyrulnik - Murmurul fantomelor 0.5 '{Diverse}.docx

./Boris Druta:
Boris Druta - Acoperis deasupra ploii 0.99 '{Literatura}.docx
Boris Druta - Arabescuri 0.7 '{Literatura}.docx
Boris Druta - Catacomba 0.7 '{Literatura}.docx
Boris Druta - Copiii serenadelor 0.8 '{Literatura}.docx
Boris Druta - Doroteea 0.9 '{Literatura}.docx
Boris Druta - Flori de dor pentru Salom 0.7 '{Literatura}.docx
Boris Druta - Luciana 0.9 '{Literatura}.docx
Boris Druta - O femeie isi cauta pantalonii 0.6 '{Literatura}.docx
Boris Druta - Romanul copilariei antice 0.9 '{Literatura}.docx

./Boris Fradkin:
Boris Fradkin - Prizonierii beznei de foc 0.99 '{SF}.docx

./Boris Ioachim:
Boris Ioachim - Lantul care ne dezleaga 1.0 '{Literatura}.docx

./Boris Pasternak:
Boris Pasternak - Doctor Jivago 2.0 '{Literatura}.docx

./Boris Polevoi:
Boris Polevoi - Povestea unui om adevarat 3.0 '{ActiuneRazboi}.docx

./Boris Vasile Malschi:
Boris Vasile Malschi - Intoarcerea lui Lica Ciocarlan 0.9 '{Literatura}.docx

./Boris Vian:
Boris Vian - Blues pentru o pisica neagra 1.0 '{Literatura}.docx
Boris Vian - Iarba rosie 0.9 '{Literatura}.docx
Boris Vian - N-as prea vrea ca s-o mierlesc 0.9 '{Literatura}.docx
Boris Vian - Smulgatorul de inimi 0.7 '{Literatura}.docx
Boris Vian - Spuma zilelor 1.0 '{Literatura}.docx
Boris Vian - Voi scuipa pe mormintele voastre 1.0 '{Literatura}.docx

./Boris Von Smercek:
Boris Von Smercek - Al doilea Graal 1.0 '{AventuraIstorica}.docx

./Bradley Nelson:
Bradley Nelson - Codul emotiilor 0.8 '{Spiritualitate}.docx

./Brad Meltzer:
Brad Meltzer - Beecher White - V1 Cercul asasinilor 1.0 '{Thriller}.docx
Brad Meltzer - Beecher White - V2 Cercul cavalerilor 1.0 '{Thriller}.docx
Brad Meltzer - Cartea sortii 1.0 '{Thriller}.docx
Brad Meltzer - Jocul zero 1.0 '{Thriller}.docx
Brad Meltzer - Milionarii 1.0 '{Thriller}.docx

./Brad Thor:
Brad Thor - Scot Harvath - V5 Asediu 2.0 '{Suspans}.docx
Brad Thor - Scot Harvath - V12 Lista neagra 1.0 '{Suspans}.docx
Brad Thor - Scot Harvath - V13 Act de razboi 1.0 '{Suspans}.docx
Brad Thor - Scot Harvath - V17 Codul de conduita 1.0 '{Suspans}.docx

./Brad Ward:
Brad Ward - Seriful din Medicine Bend 1.0 '{Western}.docx

./Bram Stocker:
Bram Stocker - Dracula 3.1 '{Horror}.docx

./Bram Stoker:
Bram Stoker - Rubinul cu sapte stele 1.0 '{Horror}.docx

./Brandon Bays:
Brandon Bays - Calatoria 1.0 '{Spiritualitate}.docx

./Brandon Sanderson:
Brandon Sanderson - Arhiva Luminii de Furtuna - V2 Cuvinte despre Lumina - V1 Campiile sfaramate 1.0 '{SF}.docx
Brandon Sanderson - Arhiva Luminii de Furtuna - V2 Cuvinte despre Lumina - V2 Poarta juramantului 1.0 '{SF}.docx
Brandon Sanderson - Calea regilor V1 1.0 '{SF}.docx
Brandon Sanderson - Calea regilor V2 1.0 '{SF}.docx
Brandon Sanderson - Elantris 2.0 '{SF}.docx
Brandon Sanderson - Nascuti in Ceata - V1 Ultimul imperiu 1.0 '{SF}.docx
Brandon Sanderson - Nascuti in Ceata - V2 Fantana inaltarii 2.0 '{SF}.docx
Brandon Sanderson - Nascuti in Ceata - V3 Eroul evurilor 1.0 '{SF}.docx
Brandon Sanderson - Nascuti in Ceata - V4 Aliati in slujba dreptatii 1.0 '{SF}.docx
Brandon Sanderson - Nascuti in Ceata - V5 Umbra Sinelui 1.0 '{SF}.docx
Brandon Sanderson - Razbunatorii - V1 Steelheart 1.0 '{SF}.docx
Brandon Sanderson - Sufletul imparatului 1.0 '{SF}.docx
Brandon Sanderson - Umbra Sinelui 1.0 '{SF}.docx

./Brendan Dubois:
Brendan Dubois - Steaua cazatoare 0.7 '{SF}.docx

./Brendan Duffy:
Brendan Duffy - Regele furtunii 1.0 '{Literatura}.docx

./Brent Weeks:
Brent Weeks - Ingerul Noptii - V1 Asasinul din umbra 1.0 '{SF}.docx

./Bret Easton Ellis:
Bret Easton Ellis - Lunar Park 0.99 '{Diverse}.docx

./Bret Harte:
Bret Harte - Ce aduce uraganul 2.0 '{Western}.docx
Bret Harte - Coliba blestemata 2.0 '{Western}.docx
Bret Harte - Fiul pistolarului 1.0 '{Western}.docx
Bret Harte - Prietenul meu, vagabondul 2.0 '{Western}.docx
Bret Harte - Prima familie din Tasajara 2.0 '{Western}.docx
Bret Harte - Surghiunitii din Poker Flat 1.0 '{Western}.docx

./Brett Daniels:
Brett Daniels - Teroare in Arkansas 1.0 '{ActiuneRazboi}.docx

./Brian Aldiss:
Brian Aldiss - Cryptozoic 2.0 '{SF}.docx
Brian Aldiss - Greybeard 1.0 '{SF}.docx
Brian Aldiss - Helliconia - V1 Primavara 1.1 '{SF}.docx
Brian Aldiss - Helliconia - V2 Vara 1.0 '{SF}.docx
Brian Aldiss - Noaptea in care a avut loc marea scurgere de timp 0.99 '{SF}.docx
Brian Aldiss - Sera 5.0 '{SF}.docx

./Brian Herbert & Kevin J. Anderson:
Brian Herbert & Kevin J. Anderson - Legende - V1 Jihadul Butlerian 4.0 '{SF}.docx
Brian Herbert & Kevin J. Anderson - Legende - V2 Cruciada masinilor 1.0 '{SF}.docx
Brian Herbert & Kevin J. Anderson - Legende - V3 Batalia Corrinului 1.0 '{SF}.docx
Brian Herbert & Kevin J. Anderson - Preludiul Dunei - V1 Casa Atreides 4.0 '{SF}.docx
Brian Herbert & Kevin J. Anderson - Preludiul Dunei - V2 Casa Harkonnen 4.0 '{SF}.docx
Brian Herbert & Kevin J. Anderson - Preludiul Dunei - V3 Casa Corrino 4.0 '{SF}.docx

./Brian Ruckley:
Brian Ruckley - Lumea fara Zei - V1 Nasterea iernii 1.0 '{SF}.docx

./Brian Sanderson:
Brian Sanderson - Elantris 1.0 '{SF}.docx

./Brian Stableford:
Brian Stableford - Invazia de pe marte 0.99 '{SF}.docx

./Brian Tracy:
Brian Tracy - Incepe cu ce nu-ti place 1.0 '{DezvoltarePersonala}.docx
Brian Tracy - Succesul in viata 0.9 '{DezvoltarePersonala}.docx

./Brian W. Taylor:
Brian W. Taylor - Supravietuim 1.0 '{SF}.docx

./Brigitte Hamann:
Brigitte Hamann - Elisabeth, imparateasa fara voie 2.0 '{Istorie}.docx

./Brittainy Cherry:
Brittainy Cherry - Aerul pasiunii 1.0 '{Romance}.docx

./Bro David Paul:
Bro David Paul - Reiky Rainbows 0.2 '{Spiritualitate}.docx

./Brodie Richard:
Brodie Richard - Virusul mintii. Memetica 0.99 '{Psihologie}.docx

./Brooke Davis:
Brooke Davis - Obiecte pierdute 1.0 '{Literatura}.docx

./Bruce Golden:
Bruce Golden - Vremuri obisnuite 0.99 '{SF}.docx

./Bruce Mcallister:
Bruce Mcallister - Comando psi 0.99 '{ActiuneComando}.docx
Bruce Mcallister - Inrudire 2.0 '{SF}.docx

./Bruce Sterling:
Bruce Sterling - Mecanicul de biciclete 0.99 '{SF}.docx
Bruce Sterling - Strigoiul 0.99 '{SF}.docx

./Bruce Wallace:
Bruce Wallace - Arme pentru asasini 1.0 '{ActiuneComando}.docx
Bruce Wallace - Razbunarea justitiarilor 1.0 '{ActiuneComando}.docx

./Bruce Wilkinson:
Bruce Wilkinson - Cele 7 legi ale invatarii 0.9 '{DezvoltarePersonala}.docx
Bruce Wilkinson - Viata pe care Dumnezeu o rasplateste 0.99 '{Spiritualitate}.docx

./Bruno Ferrero:
Bruno Ferrero - Cantecul unui greier 0.99 '{SF}.docx
Bruno Ferrero - Cercuri in apa 0.99 '{SF}.docx
Bruno Ferrero - Doar vantul o stie 0.99 '{SF}.docx
Bruno Ferrero - E cineva acolo sus 0.99 '{SF}.docx
Bruno Ferrero - Secretul pestilor rosii 0.99 '{SF}.docx
Bruno Ferrero - Trandafirul de mare pret 0.99 '{SF}.docx

./Bruno Henriquez:
Bruno Henriquez - Chef de glume 5.0 '{SF}.docx

./Bruno Schulz:
Bruno Schulz - Manechinele 1.0 '{Literatura}.docx

./Bucura Dumbrava:
Bucura Dumbrava - Pandurul 1.0 '{IstoricaRo}.docx

./Bucur Chiriac:
Bucur Chiriac - Anotimpul linistii 0.99 '{Literatura}.docx
Bucur Chiriac - Spovedania unui colectionar de arta 0.9 '{Literatura}.docx

./Bulat Okudjava:
Bulat Okudjava - Calatoria diletantilor 1.0 '{Dragoste}.docx
Bulat Okudjava - Intalnire cu Bonaparte 1.0 '{AventuraIstorica}.docx

./Burt Hirschfeld:
Burt Hirschfeld - Dallas - V1 Familia Ewing 1.0 '{Thriller}.docx
Burt Hirschfeld - Dallas - V2 Femeile Ewing 1.0 '{Thriller}.docx
Burt Hirschfeld - Dallas - V3 Barbatii Ewing 1.0 '{Thriller}.docx
```

